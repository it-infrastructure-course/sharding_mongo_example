# Notes
The purpose of this lab is to show the benefits of sharding.

You must be familiar with:
- screen (only required for shards)
- jmeter and plugin ThroughputShapingTimer
- ssh
- terminal text editor such as: vi or nano

# Install Single DB

## Install dependencies
```
sudo snap install node --classic
npm install --save express
npm install --save qr-image
npm install --save crypto
npm install --save mongodb

```
## Install mongodb server and create database and collection

### Install on Ubuntu 22.04 LTS
```
sudo apt-get update
sudo apt-get install gnupg curl
curl -fsSL https://www.mongodb.org/static/pgp/server-7.0.asc | sudo gpg -o /usr/share/keyrings/mongodb-server-7.0.gpg --dearmor
echo "deb [ arch=amd64,arm64 signed-by=/usr/share/keyrings/mongodb-server-7.0.gpg ] https://repo.mongodb.org/apt/ubuntu jammy/mongodb-org/7.0 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-7.0.list
sudo apt-get update
sudo apt-get install -y mongodb-org
```
### Configure
Allow access from extranal hosts in `/etc/mongod.conf` and set `bindIp` to
```
bindIp: 0.0.0.0
```
Adapt firewall settings if required for port 27017.

### Performance tweaks
Add following lines to `/etc/security/limits.conf`
```
*                soft    nofile          64000
*                hard    nofile          64000
*                soft    nproc           unlimited
*                hard    nproc           unlimited
```
and restart the host.

### Start mongod
```
sudo systemctl start mongod
```

Create database and collection using db client `mongosh`, after connecting to the db do
```
use users
db.createCollection("user")
```

If you need to remove all documents, use db client `mongosh`, after connecting to the db do
```
use users
db.user.remove({})
```

## Configure and start the app
Configure the IP address of the DB server in `sharding_mongo_example.js` and start the app

```
node sharding_mongo_example.js
```

## Test the app
Install JMeter and plugin ThroughputShapingTimer, if not already available.

The tests creates 10000 users and afterwards request users with base64 encoded QR code images that contain a substring of random characters.
```
# Remove result file and report directory if exist
# rm -rf result.jtl report
jmeter -n -t ./tests/create_update.jmx -l result.jtl -e -o ./report
# View report firefox report/index.html
```

# Install, Configure and Start Shards (optional)

Create overall six nodes with mongodb installed as described previously. Ensure the service is stopped `sudo systemctl stop mongod` for the next steps. The mongod and mongos programs are executed inside a screen to see log information in this example.

Installation and configuration only have to be done once.

The next steps summarize the commands to create a clean setup. Do NOT remove directories if you only restart mongod.

## Stop any existing serive

Previously mongodb was managed by systemd as a service.

Here, we start mongodb from the shell, therefore you have to stop any previos mongodb service with

```
sudo systemctl stop mongod
sudo systemctl disable mongod
```

To prevent that the serive stop when we disconnect the ssh connection, we put the process into a `screen`.

## Install two Config Server
```
CONFSVR1=ec2-35-157-91-217.eu-central-1.compute.amazonaws.com
ssh -i ~/Downloads/awseducate.pem $CONFSVR1 -l ubuntu
rm -rf ~/data/configdb
mkdir -p ~/data/configdb
screen
sudo sh -c "echo 'never' | tee /sys/kernel/mm/transparent_hugepage/enabled"
mongod --configsvr --replSet ConfigReplSet --dbpath ~/data/configdb --bind_ip 0.0.0.0
```

Exit from the screen and the host.

```
CONFSVR2=ec2-54-93-233-100.eu-central-1.compute.amazonaws.com
ssh -i ~/Downloads/awseducate.pem $CONFSVR2 -l ubuntu
rm -rf ~/data/configdb
mkdir -p ~/data/configdb
screen
sudo sh -c "echo 'never' | tee /sys/kernel/mm/transparent_hugepage/enabled"
mongod --configsvr --replSet ConfigReplSet --dbpath ~/data/configdb --bind_ip 0.0.0.0
```

Exit from the screen and the host.

## Setup config replica set

Login to the mongo shell on one config server:
```
CONFSVR1_INT=172.31.38.101
mongo --host ${CONFSVR1_INT} --port 27019
```
Create the replica set:

```
rs.initiate(
  {
    _id: "ConfigReplSet",
    configsvr: true,
    members: [
      { _id : 0, host : "172.31.38.101:27019" },
      { _id : 1, host : "172.31.44.249:27019" }
    ]
  }
)
```
Exit from shell and host.

## Start shard servers

Start the first shard server.

```
SHARDSVR1=ec2-54-93-32-180.eu-central-1.compute.amazonaws.com
ssh -i ~/Downloads/awseducate.pem $SHARDSVR1 -l ubuntu
rm -rf ~/data/sharddb
mkdir -p ~/data/sharddb
screen
sudo sh -c "echo 'never' | tee /sys/kernel/mm/transparent_hugepage/enabled"
mongod --shardsvr --replSet DataReplSet1 --dbpath ~/data/sharddb --bind_ip 0.0.0.0
```https://jmeter-plugins.org/wiki/ThroughputShapingTimer/

Login to mongod:

```
shard_member=localhost
mongo --host ${shard_member} --port 27018
```

Create the first replica set:https://jmeter-plugins.org/wiki/ThroughputShapingTimer/
rs.initiate(
  {
    _id : "DataReplSet1",
    members: [
      { _id : 0, host : "172.31.41.25:27018" }
    ]
  }
)
```

Start the second shard server:

```
SHARDSVR2=ec2-3-124-242-60.eu-central-1.compute.amazonaws.com
ssh -i ~/Downloads/awseducate.pem $SHARDSVR2 -l ubuntu
rm -rf ~/data/sharddb
mkdir -p ~/data/sharddb
screen
sudo sh -c "echo 'never' | tee /sys/kernel/mm/transparent_hugepage/enabled"
mongod --shardsvr --replSet DataReplSet2 --dbpath ~/data/sharddb --bind_ip 0.0.0.0 
```

Exit from the screen and login to the DB: 

```
shard_member=localhost
mongo --host ${shard_member} --port 27018
```

Create the second replica set:

```
rs.initiate(
  {
    _id : "DataReplSet2",
    members: [
      { _id : 0, host : "172.31.33.23:27018" }
    ]
  }
)
```

# Start mongos server

Start the mongos server and connect it to the config servers.

```
MONGOSSVR=ec2-3-125-42-219.eu-central-1.compute.amazonaws.com
ssh -i ~/Downloads/awseducate.pem $MONGOSSVR -l ubuntu
screen
CONFSVR1_INT=172.31.38.101
CONFSVR2_INT=172.31.44.249
sudo sh -c "echo 'never' | tee /sys/kernel/mm/transparent_hugepage/enabled"
mongos --configdb ConfigReplSet/$CONFSVR1_INT:27019,$CONFSVR2_INT:27019 --bind_ip 0.0.0.0
```

Exit from the screen and connect to the mongos DB:

```
mongos_host=localhost
mongo --host ${mongos_host} --port 27017
```

Add the shards and enabling shardung for the collection users:

```
sh.addShard( "DataReplSet1/172.31.41.25:27018")
sh.addShard( "DataReplSet2/172.31.33.23:27018")
sh.enableSharding("users")
sh.shardCollection("users.user", { _id : "hashed" } )
```

Redirect the node.js app to the mongos service and test again with jmeter for comparison.
