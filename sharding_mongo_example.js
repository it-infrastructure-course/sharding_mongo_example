var express = require('express');
const crypto = require('crypto');
var qr = require('qr-image');
var MongoClient = require('mongodb').MongoClient;
var mo_url = 'mongodb://rlmdb.wi.fh-flensburg.de:27017/users';
var assert = require('assert');


// Share connection for improvement https://blog.fullstacktraining.com/sharing-a-mongodb-connection-in-node-js-express/

// Create a hash, it will change each hour.
function getHash(data) {
	var date = new Date();
	var current_hour = date.getHours();
  var data = crypto.createHash("sha512").update(data, "binary").digest("hex")
  return crypto.createHash("sha512").update(data + current_hour.toString(), "binary").digest("hex");
}

var app = express();

app.get('/', function (req, res) {
	console.log("Request from: " + req.ip)
	res.send("Use http://<hostname>/hash/<id>");
});
mc_opt =  { }


app.get('/hash/:id', function (req, res) {
  console.log("Create hash from: " + req.ip);
  var hash=getHash(req.params.id);
  var qr_png = qr.imageSync(hash,{ type: 'png'});
  var q = {"_id": req.params.id};
  var v = {$set: {hash: hash, png: qr_png.toString('base64')} };
  var up_opt = { upsert: true };
  
	console.log("Connect to db " + mo_url);
  //var mongoclient = new MongoClient(mo_url);
  MongoClient.connect(mo_url).then(
    (db) =>  {
      var dbo = db.db("users");
      dbo.collection('user').updateOne(q,v,up_opt).then(
        (value) => {
          console.log(req.params.id + value);
          res.write('<html><head></head><body>');
          res.write('<p>' +  hash + '</p>');
          res.write('<img src="data:image/png;base64,' + qr_png.toString('base64') + '" alt="QR Code">');
          res.end('</body></html>');
          db.close();
        },
        (reason) => {
          console.error(reason);
          db.close();
        })},
    (reason) => {
      console.error(reason);
    } );
})


app.get('/gethash/:id', function (req, res) {
  console.log("Get hashes from: " + req.ip);
  let q = { _id: {$regex : req.params.id } };
  var MongoClient = require('mongodb').MongoClient;
	MongoClient.connect(mo_url, mc_opt).then(
    (db) =>  {
      var dbo = db.db("users");
      dbo.collection('user').find(q).toArray().then(
        (result) => {
          console.log(result);
          res.json(result);
          db.close();
        },
        (reason) => {
          console.error(reason); // Error!
          db.close();
        })},
    (reason) => {
      console.error(reason); // Error!
    } );
})
  

app.listen(3000, "0.0.0.0", function () {
  console.log('Example app listening on port 3000!');
});


